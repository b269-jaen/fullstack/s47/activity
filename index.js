// "document" refers to the whole webpage
// "querySelector" is used to select a specific object (HTML element) from our document (webpage)
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name")
const spanFullName = document.querySelector("#span-full-name");
const nameForm = document.querySelector("#name-form")

// document.getElementByID()
// document.getElementByClassName()

// const txtFirstName = document.getElementByID("txt-first-name");
// const spanFullName = document.getElementByID("span-full-name");

// Whenever a user intereacts with a web page, this action is considered as an event
// "addEventListener" takes two argument
// "keyup" - string identifying an event
// function that the listener will execute once the specified event is triggered
nameForm.addEventListener('keyup', (event) => {
	// "innerHTML" property sets or returns the HTML content 
	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
	// "event.target" contains the element where the event happened
	console.log(event.target);
	// "event.target.value" gets the vlue of the input object
	console.log(event.target.value);
});
